#include "node.h"
#include "context.h"
#include "error.h"
#include <set>

Node::Node(const unsigned n, SimulationContext *c, double b, double l) : 
    number(n), context(c), bw(b), lat(l)
{
  table = new Table();
}

Node::Node() 
{ throw GeneralException(); }

Node::Node(const Node &rhs) : 
  number(rhs.number), context(rhs.context), bw(rhs.bw), lat(rhs.lat) {
  table = new Table(*rhs.table);
}

Node & Node::operator=(const Node &rhs) 
{
  return *(new(this)Node(rhs));
}

void Node::SetNumber(const unsigned n) 
{ number=n;}

unsigned Node::GetNumber() const 
{ return number;}

void Node::SetLatency(const double l)
{ lat=l;}

double Node::GetLatency() const 
{ return lat;}

void Node::SetBW(const double b)
{ bw=b;}

double Node::GetBW() const 
{ return bw;}

Node::~Node()
{}

// Implement these functions  to post an event to the event queue in the event simulator
// so that the corresponding node can recieve the ROUTING_MESSAGE_ARRIVAL event at the proper time
void Node::SendToNeighbors(const RoutingMessage *m)
{
  deque<Node*> *neighbors = GetNeighbors();
  deque<Node*>::iterator it = neighbors->begin();
  while (it != neighbors->end()){
    SendToNeighbor(*it++, m);
  }
}

void Node::SendToNeighbor(const Node *n, const RoutingMessage *m)
{
  cerr << endl << "SendToNeighbor: " << *n << *m << endl;
  Link *l = new Link();
  l->SetDest(n->number);
  l->SetSrc(this->number);
  Link * tl = new Link(*l);
  Link * rl = context->FindMatchingLink(tl);
  if(rl){
    Event *e = new Event(rl->GetLatency()+context->GetTime(),(EventType)10,const_cast<Node *> (n),const_cast<RoutingMessage *> (m));
    context->PostEvent(e);
  }
  delete l;
  delete tl;
}

deque<Node*> *Node::GetNeighbors()
{
  return context->GetNeighbors(this);
}

void Node::SetTimeOut(const double timefromnow)
{
  context->TimeOut(this,timefromnow);
}


bool Node::Matches(const Node &rhs) const
{
  return number==rhs.number;
}


#if defined(GENERIC)
void Node::LinkHasBeenUpdated(const Link *l)
{
  cerr << *this << " got a link update: "<<*l<<endl;
  //Do Something generic:
  SendToNeighbors(new RoutingMessage);
}


void Node::ProcessIncomingRoutingMessage(const RoutingMessage *m)
{
  cerr << *this << " got a routing messagee: "<<*m<<" Ignored "<<endl;
}


void Node::TimeOut()
{
  cerr << *this << " got a timeout: ignored"<<endl;
}

Node *Node::GetNextHop(const Node *destination) const
{
  return 0;
}

Table *Node::GetRoutingTable() const
{
  return new Table;
}


ostream & Node::Print(ostream &os) const
{
  os << "Node(number="<<number<<", lat="<<lat<<", bw="<<bw<<")";
  return os;
}

#endif

#if defined(LINKSTATE)


void Node::LinkHasBeenUpdated(const Link *l)
{
  cerr << *this<<": Link Update: "<<*l<<endl;
  SendToNeighbors(new RoutingMessage(l->GetSrc(),l->GetDest(),l->GetLatency(),table->UpdateLink(l)));
}


void Node::ProcessIncomingRoutingMessage(const RoutingMessage *m)
{
  cerr << *this << " Routing Message: "<<*m<<endl;
  //@FIXME establish links with node 0
  if(table->t.find(number)==table->t.end()){
    Link *l = new Link();
    l->SetSrc(number);
    l->SetDest(m->src);
    LinkHasBeenUpdated(context->FindMatchingLink(l));
    l->SetSrc(m->src);
    l->SetDest(number);
    LinkHasBeenUpdated(context->FindMatchingLink(l));
  }
  if(table->Update(m)){
    SendToNeighbors(m);
  }
}

void Node::TimeOut()
{
  cerr << *this << " got a timeout: ignored LIKE A BAU5"<<endl;
}

Node *Node::GetNextHop(const Node *destination) const
{
  cerr << endl << "GetNextHop" << endl;
  //yay Djikstra's...
  set<int> visited, unvisited;
  map<int, int> previous;
  map<int, int> path;
  map<int, map<int, LinkState> > tablemap = (const_cast<Table *> (table))->t;
  
  //for current node
  visited.insert(number);
  for(map<int, LinkState>::iterator i=tablemap[number].begin();i!=tablemap[number].end();i++){
    unvisited.insert(i->first);
    LinkState ls = tablemap[number][i->first];
    ls.dist = ls.cost;
    ls.visited = false;
    previous[i->first] = number;
  }

  //for everything else
  while(unvisited.size()){
    //find smallest path to evaluate first
    int src = 0;
    int dest = 0;
    int maxcost = -1;
    for(map<int, map<int, LinkState> >::const_iterator i=tablemap.begin();i!=tablemap.end();i++)
      for(map<int, LinkState>::const_iterator j=i->second.begin();j!=i->second.end();j++)
        if(!j->second.visited)
          if(maxcost < 0 || maxcost < j->second.dist){
            cerr << "finding src/dest";
            sleep(1);
            src = i->first;
            dest = j->first;
            maxcost = j->second.dist;
          }
    
    cerr << unvisited.size() << src << dest << endl;
    //skip visited nodes
    if(visited.count(dest)){
      tablemap[src][dest].visited = true;
      continue;
    }
    //continue for all nodes
    visited.insert(dest);
    previous[dest] = src;
    unvisited.erase(dest);
    for(map<int, LinkState>::const_iterator i=tablemap[dest].begin();i!=tablemap[dest].end();i++){
      if(!visited.count(i->first)){
        unvisited.insert(i->first);
        LinkState ls = tablemap[dest][i->first];
        //pick shortest route
        int pathCost = ls.cost+tablemap[src][dest].dist;
        if(ls.dist == -1)       ls.dist = pathCost;
        else if(pathCost != -1) ls.dist = (ls.dist < pathCost)?ls.dist:pathCost;
      }
      previous[i->first] = number;
    }
  }
  for(map<int, int>::const_iterator i=previous.begin();i!=previous.end();i++){
    int node = i->first;
    int prev = i->second;
    while(prev != node){
        node = prev;
        prev = previous[prev];
    }
    path[i->first] = node;
  }
  cerr << "djikstra done~" << endl;
  Node *n = new Node(path[destination->number],NULL,0,0);
  return context->FindMatchingNode(const_cast<Node *> (n));
}

Table *Node::GetRoutingTable() const
{
  cerr << "GetRoutingTable" << endl;
  return new Table(*table);
}


ostream & Node::Print(ostream &os) const
{
  os << "Node(number="<<number<<", lat="<<lat<<", bw="<<bw<<", table="<<*table<<")";
  return os;
}
#endif


#if defined(DISTANCEVECTOR)

void Node::LinkHasBeenUpdated(const Link *l)
{
  // update our table
  // send out routing mesages
  cerr << *this<<": Link Update: "<<*l<<endl;
}


void Node::ProcessIncomingRoutingMessage(const RoutingMessage *m)
{
  //TODO process!!
  SendToNeighbors(m);
}

void Node::TimeOut()
{
  cerr << *this << " got a timeout: ignored"<<endl;
}


Node *Node::GetNextHop(const Node *destination) const
{
  //FIXME return context->FindMatchingNode(Node(table.
}

Table *Node::GetRoutingTable() const
{
  //is this needed??
  return NULL;
}


ostream & Node::Print(ostream &os) const
{
  os << "Node(number="<<number<<", lat="<<lat<<", bw="<<bw;
  return os;
}
#endif
