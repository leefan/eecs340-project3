#include "table.h"
#include "messages.h"

#if defined(GENERIC)
ostream & Table::Print(ostream &os) const
{
  // WRITE THIS
  os << "Table()";
  return os;
}
#endif

#if defined(LINKSTATE)
ostream & Table::Print(ostream &os) const{
  os << "Table";
  map<int, map<int, LinkState> >::const_iterator it = t.begin();
  os << "(";
  for(;it != t.end();it++){
    os << it->first << ":";
    map<int, LinkState>::const_iterator itb = it->second.begin();
    for(;itb != it->second.end();itb++)
      os << itb->first << "(" << itb->second.cost << "," << itb->second.dist << "," << itb->second.ver << ");";
  }
  os << ")";
  return os;
}

bool Table::Update(const RoutingMessage *m){
  if(t[m->src][m->dest].ver < m->ver ||
       t[m->src][m->dest].cost == -1){
    t[m->src][m->dest].cost = m->cost;
    t[m->src][m->dest].ver = m->ver;
    cerr << "<TABLEUPDATED>" << *this;
    return true;
  }return false;
}

Table::Table() : t(){
}

Table::Table(const Table &table){
  for(map<int, map<int, LinkState> >::const_iterator i=table.t.begin();i!=table.t.end();i++){
    t[i->first];
    for(map<int, LinkState>::const_iterator j=i->second.begin();j!=i->second.end();j++)
      (t[i->first])[j->first].Copy(j->second);
  }
}

int Table::UpdateLink(const Link *l){
  cerr << endl <<  "UpdateLink: " << *this << endl;
  t[l->GetSrc()][l->GetDest()].ver = t[l->GetSrc()][l->GetDest()].ver + 1;
  t[l->GetSrc()][l->GetDest()].cost = l->GetLatency();
  t[l->GetSrc()][l->GetDest()].dist = l->GetLatency();
  return t[l->GetSrc()][l->GetDest()].ver;
}

void LinkState::Copy(const LinkState &ls){
  cost = ls.cost;
  dist = ls.dist;
  ver = ls.ver;
  visited = ls.visited;
}

LinkState::LinkState(){
  cost = -1;
  dist = -1;
  ver = 0;
  visited = false;
}

LinkState::LinkState(int c, int v){
  cost = c;
  ver = v;
  dist = -1;
  visited = false;
}
#endif

#if defined(DISTANCEVECTOR)


#endif
