#include "messages.h"


#if defined(GENERIC)
ostream &RoutingMessage::Print(ostream &os) const
{
  os << "RoutingMessage()";
  return os;
}
#endif


#if defined(LINKSTATE)

ostream &RoutingMessage::Print(ostream &os) const
{
  os << "RoutingMessage(source=" << src << ", destination=" << dest << ", cost=" << cost << ", version=" << ver << ")";
  return os;
}

RoutingMessage::RoutingMessage()
{
  //default constructor, shouldn't be called ever
}

RoutingMessage::RoutingMessage(const RoutingMessage &rhs)
{}

RoutingMessage::RoutingMessage(int s, int d, int c, int v)
{
  src = s;
  dest = d;
  cost = c;
  ver = v;
}
#endif


#if defined(DISTANCEVECTOR)

ostream &RoutingMessage::Print(ostream &os) const
{
  return os;
}

RoutingMessage::RoutingMessage()
{}


RoutingMessage::RoutingMessage(const RoutingMessage &rhs):src(rhs.src) //TODO finish constructor
{}

#endif

