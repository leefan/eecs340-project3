#ifndef _table
#define _table


#include <iostream>
#include "messages.h"

using namespace std;

#if defined(GENERIC)
class Table {
  // Students should write this class

 public:
  ostream & Print(ostream &os) const;
};
#endif


#if defined(LINKSTATE)
#include <map>
class LinkState{
 public:
  int cost, dist, ver;
  bool visited;
  LinkState();
  void Copy(const LinkState &ls);
  LinkState(int c, int v);
};

class Table {
  // Students should write this class
 public:
  Table();
  Table(const Table &table);
  ostream & Print(ostream &os) const;
  bool Update(const RoutingMessage *m);
  int UpdateLink(const Link *l);
  map<int, map<int, LinkState> > t;
  //map should probably be private, but since the rest of the skeleton
  //isn't encapsulating properly, I shouldn't be expected to either
};
#endif

#if defined(DISTANCEVECTOR)

#include <deque>
#include <vector>

class Table {
 public:
  ostream & Print(ostream &os) const;
 private:
  vector<vector<double> > costTable;
};
#endif

inline ostream & operator<<(ostream &os, const Table &t) { return t.Print(os);}

#endif
